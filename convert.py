import pickle
import skl2onnx
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType

with open('/home/don/Documents/Sam/mdot-backend/mdot-server/kalman_rssi_regression_model.pkl', 'rb') as f:
    model = pickle.load(f)

initial_type = [('float_input', FloatTensorType([None, model.n_features_in_]))]
onnx_model = convert_sklearn(model, initial_types=initial_type)

with open('model.onnx', 'wb') as f:
    f.write(onnx_model.SerializeToString())
