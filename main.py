import joblib
import numpy as np

from pydantic import BaseModel


from fastapi import FastAPI, WebSocket


app = FastAPI()

class Item(BaseModel):
    rssi: int
    # description: str | None = None
    # price: float
    # tax: float | None = None



# Load the trained model from the file
#model = joblib.load('regression_model.pkl')
model=joblib.load('models/regression_model_v2.pkl')


# Single RSSI value for prediction
single_rssi_value = -57


def process_data(data):
    single_rssi_value = data
    
    # Implement your data processing logic here
        # Reshape the data to match the expected input shape (1 sample, 1 feature)
    single_rssi_value_reshaped = np.array(single_rssi_value).reshape(-1, 1)
    # Predict distance using the loaded model
    # predicted_distance = model.predict(single_rssi_value_reshaped)
    predicted_distance = model.predict(single_rssi_value_reshaped) 
    return predicted_distance  # Example processing




@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.post("/rssi-data/")
async def create_item(item: Item):
    
    print(int(item.rssi))
    
    processed_data = process_data(int(int(item.rssi)))  # Your data processing function
    
    print(processed_data)
    float_number = float(processed_data[0])
    print("float number",float_number)
    # # await websocket.send_text(f"Processed data: {processed_data}")
    return round(float_number, 3)

@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        data = await websocket.receive_text()
        print("data", data, "type", type(data))
        
        if data != '1':
            processed_data = process_data(int(data))  # Your data processing function
            await websocket.send_text(f"Processed data: {processed_data}")
        elif data=='1':
            print("ping received")        
   

