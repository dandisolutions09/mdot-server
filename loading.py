import joblib
import numpy as np

# Load the trained model from the file
model = joblib.load('kalman_rssi_regression_model.pkl')

# Single RSSI value for prediction
single_rssi_value = -47

# Reshape the data to match the expected input shape (1 sample, 1 feature)
single_rssi_value_reshaped = np.array(single_rssi_value).reshape(-1, 1)

# Predict distance using the loaded model
predicted_distance = model.predict(single_rssi_value_reshaped) * -1

print(f"Predicted distance for RSSI value {single_rssi_value}: {predicted_distance[0]}")